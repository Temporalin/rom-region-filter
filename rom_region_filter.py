import os
import re


def main():
    bak_dir = 'bak'

    os.makedirs(bak_dir, exist_ok=True)

    files = os.listdir()
    patt = re.compile(r'(.*?) \((.*?)\)(.*)?\.(\w+)')

    regions = set()

    usa_roms = []
    non_usa_roms = []
    for file in files:
        match = patt.match(file)
        if match:
            print(f'{file}')
            groups = match.groups()

            game_name, region, other, extension = groups
            for group in groups:
                group = group if group != '' else '---'
                print(f'\t {group}')
            regions.add(region)

            if 'USA' in region:
                usa_roms.append((file, game_name))
            else:
                non_usa_roms.append((file, game_name))

    print('Regions:')
    for region in regions:
        print(f'- {region}')
    print()

    print('USA games:')
    for game in usa_roms:
        print(f'- {game}')
    print()

    print('Games not in USA:')
    for file, game in [(file, game) for file, game in non_usa_roms if game not in [game for file, game in usa_roms]]:
        print(f'- {game}')
    print('Games in both:')
    for file, game in [(file, game) for file, game in non_usa_roms if game in [game for file, game in usa_roms]]:
        os.system(f'move "{file}" "{os.path.join(bak_dir, file)}"')
    print()


if __name__ == '__main__':
    main()
